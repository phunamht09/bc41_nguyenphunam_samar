// JS cho phần Portfolio: lấy trên trang https://codepen.io/Khloe1425/pen/ZEejvVm => copy đoạn code mẫu bên JS vào đây:
lightGallery(document.getElementById("animated-thumbnails"), {
  selector: ".lightimg",
  plugins: [lgZoom, lgThumbnail, lgAutoplay, lgFullscreen, lgShare],
  alignThumbnails: "left",
  loop: true,
  allowMediaOverlap: true,
  toggleThumb: true,
  showZoomInOutIcons: true,
  actualSize: false,
  exThumbImage: "data-exthumbimage"
});


// JS cho phần Testimonial: lấy trên trang https://codepen.io/Khloe1425/pen/rNyommd?fbclid=IwAR3Fkw31yMLKergbVfYn3Gs9aiCXsiy7rfZ2aNmEA1oLCrTTmxpqZTnDc1U => copy đoạn code mẫu bên JS vào đây:

var testimonialThumbs = new Swiper(".testimonial__thumbs", {
  spaceBetween: 10,
  slidesPerView: 3,
  centeredSlides: true,
  freeMode: true,
  autoplay: true,
  loop: true,
  speed: 3000
});
var testimonialContent = new Swiper(".testimonial__comment", {
  spaceBetween: 10,
  autoplay: true,
  loop: true,
  speed: 3000,
  thumbs: {
    swiper: testimonialThumbs
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true
  }
});
